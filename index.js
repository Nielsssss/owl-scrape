const request = require("request");
const JSSoup = require("jssoup").default;
//TODO Take input from CSV instead of array 
const arr = [
  "87seconds"
];

function companyRequestOptions(company) {
  return {
    method: "GET",
    url: `https://www.owler.com/company/${company}`,
    headers: {
      Host: "www.owler.com",
      Connection: "keep-alive",
      "User-Agent":
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36",
      "Sec-Fetch-Mode": "navigate",
      "Sec-Fetch-User": "?1",
      Accept:
        "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
      "Sec-Fetch-Site": "same-origin",
      Referer: "https://www.owler.com/about",
      //"Accept-Encoding": "gzip, deflate, br",
      "Accept-Language": "en-US,en;q=0.9",
      Cookie:
        "D_IID=D60E3E6E-8164-393A-9012-91CA4E454B0D; D_UID=672A092C-7CB2-3674-940D-733B97295AA1; D_ZID=32F871F4-4189-3506-8ACE-58CEBD77E725; D_ZUID=1393FBB0-FD1D-3E2B-955C-7442F052BC7A; D_HID=73E59CE5-AD3F-3944-B9D6-303F3EA6074C; D_SID=157.97.112.146:A4hn0ITDYxmZU/AgVFlNMXzSDxxGzPBiUX9aCeRABtk; vid=rBEAAl1vzJ8cHQBKPYBeAg==; NC_VARNISH=true; amplitude_id_e102edba5e9caea6b89e3c04fac87a4dowler.com=eyJkZXZpY2VJZCI6IjQ1NzA5MzMxLWQ5ZTItNDhhMy1iZDQ0LTU4NGJmYjg0Y2Q0NlIiLCJ1c2VySWQiOiI1OTY0NTg1Iiwib3B0T3V0IjpmYWxzZSwic2Vzc2lvbklkIjoxNTY3NjA3OTY5ODA5LCJsYXN0RXZlbnRUaW1lIjoxNTY3NjA3OTcwNDc5LCJldmVudElkIjoxLCJpZGVudGlmeUlkIjoxLCJzZXF1ZW5jZU51bWJlciI6Mn0=; _ga=GA1.2.1127491775.1567607971; _gid=GA1.2.1877651058.1567607971; OWLER_PC=Z6yRLOHPaGVJqMUohajRUJmhUI8_FjPBhM0I66W8JLlvq_2qhUwhkqSzb2RLPQgucev-pzIn8zQagW-PjdxTJEqiAylBSPQ_FtNieOcbQiqtUeLrow_ZrSAAS4s6lwuzeuF79OLQ2oKbEYS1EO9eZw; li_c=1"
    }
  };
}

let handleBody = function(error, response, body) {
  if (error) throw new Error(error);

  let soup = new JSSoup(body);
  let competitors = soup.find("div", "carousel-wrapper");

  if (competitors) {
    let competitorsNames = competitors.findAll("a");
    console.log(
      competitorsNames.forEach(e => {
        if (e.attrs.href) {
          //TODO Write to CSV
          console.log(
            e.attrs.href.replace("https://www.owler.com/company/", "")
          );
        }
      })
    );

  } else {
    let blockTrue = soup.findAll("div");
    if (blockTrue[0].attrs.id === "distilIdentificationBlock") {
      console.log("blocked");
    }
  }
};

//TODO Break loop on block and continue after x hours
arr.forEach((arr, i) => {
  setTimeout(() => request(companyRequestOptions(arr), handleBody), 88000 * i+1);
});